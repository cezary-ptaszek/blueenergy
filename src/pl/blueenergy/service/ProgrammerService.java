package pl.blueenergy.service;

import pl.blueenergy.document.DocumentDao;
import pl.blueenergy.document.model.ApplicationForHolidays;
import pl.blueenergy.document.model.Document;
import pl.blueenergy.document.model.Question;
import pl.blueenergy.document.model.Questionnaire;
import pl.blueenergy.helper.SaveToFileHelper;
import pl.blueenergy.organization.User;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ProgrammerService {

	public void execute(DocumentDao documentDao) throws Exception {

		// task 0
		List<ApplicationForHolidays> applicationsForHolidays = splitApplicationForHolidays(documentDao);
		List<Questionnaire> questionnaires = splitQuestionnaire(documentDao);

		// task 1
		System.out.println(averageNumberOfAnswersTotal(questionnaires));

		// task 2
		List<User> users = holidaysApplicants(applicationsForHolidays);
		for(User u : users) {
			System.out.println(u.getLogin());
			System.out.println(havePolishCharacters(u));
		}

		// task 3
		for(ApplicationForHolidays a : applicationsForHolidays) {
			System.out.println(a.getSince() + " || " + a.getTo());
			System.out.println(isTimeCorrect(a));
		}

		// task 4
		File file = new File("Q&A.txt");
		file.delete();
		for(Questionnaire q : questionnaires){
			SaveToFileHelper.saveToFile(q, file);
		}

		// task 5
		System.out.println(changeSalary(applicationsForHolidays.get(0).getUserWhoRequestAboutHolidays()));

	}

	////////// QUESTIONNAIRE
	public List<Questionnaire> splitQuestionnaire(DocumentDao documentDao){
		List<Questionnaire> questionnaires = new ArrayList<>();

		for(Document d : documentDao.getAllDocumentsInDatabase()) {
			if (d instanceof Questionnaire)
				questionnaires.add((Questionnaire) d);
		}
		return questionnaires;
	}

	public double averageNumberOfAnswersTotal(List<Questionnaire> questionnaires){
		double sum = 0, total = 0;

		for(Questionnaire q : questionnaires)
			for(Question que : q.getQuestions()) {
				sum += que.getPossibleAnswers().size();
				total++;
			}
		return sum/total;
	}

	////////// APPLICATION FOR HOLIDAYS
	public List<ApplicationForHolidays> splitApplicationForHolidays(DocumentDao documentDao){
		List<ApplicationForHolidays> applicationsForHolidays = new ArrayList<>();

		for(Document d : documentDao.getAllDocumentsInDatabase()) {
			if (d instanceof ApplicationForHolidays)
				applicationsForHolidays.add((ApplicationForHolidays) d);
		}
		return applicationsForHolidays;
	}

	public List<User> holidaysApplicants(List<ApplicationForHolidays> applicationForHolidays){
		List<User> users = new ArrayList<>();
		for(ApplicationForHolidays a : applicationForHolidays){
			users.add(a.getUserWhoRequestAboutHolidays());
		}
		return users;
	}

	public boolean havePolishCharacters(User user){
		return user.getLogin().matches("^\\p{ASCII}*$");
	}

	public boolean isTimeCorrect(ApplicationForHolidays applicationForHolidays){
		return (applicationForHolidays.getTo().getTime() - applicationForHolidays.getSince().getTime() > 0);
	}

	public String changeSalary(User user) throws Exception {
		Field userSalary = User.class.getDeclaredField("salary");
		userSalary.setAccessible(true);
		userSalary.set(user, 3000.0);
		return userSalary.get(user).toString();
	}
}

package pl.blueenergy.helper;

import pl.blueenergy.document.model.Question;
import pl.blueenergy.document.model.Questionnaire;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SaveToFileHelper {

    public static void saveToFile(Questionnaire questionnaire, File file) throws IOException {
        FileWriter writer = new FileWriter(file, true);
        for(Question q : questionnaire.getQuestions()) {
            writer.write("Pytanie: " + q.getQuestionText() + "\n");
            int iterator = 1;
            for (String s : q.getPossibleAnswers()) {
                writer.write("\t" + iterator + ". " + s + "\n");
                iterator++;
            }
            writer.write("\n");
        }
        writer.close();
    }

}

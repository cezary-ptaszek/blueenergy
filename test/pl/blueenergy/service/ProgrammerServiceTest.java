package pl.blueenergy.service;

import org.junit.Test;
import pl.blueenergy.document.DocumentDao;
import pl.blueenergy.document.model.ApplicationForHolidays;
import pl.blueenergy.document.model.Questionnaire;
import pl.blueenergy.organization.User;

import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

public class ProgrammerServiceTest {

    private ProgrammerService programmerService = new ProgrammerService();
    private DocumentDao documentDao = new DocumentDao();
    private List<ApplicationForHolidays> applicationForHolidays = programmerService.splitApplicationForHolidays(documentDao);
    private List<Questionnaire> questionnaires = programmerService.splitQuestionnaire(documentDao);



    @Test
    public void averageNumberOfAnswersTotal() {
        assertEquals(4.0, programmerService.averageNumberOfAnswersTotal(questionnaires), 0.0);
    }

    @Test
    public void holidaysApplicants(){
        assertEquals("nowaczka", programmerService.holidaysApplicants(applicationForHolidays).get(1).getLogin());
    }

    @Test
    public void havePolishCharacters(){
        User user = new User();
        user.setLogin("romuś1999");

        assertFalse(programmerService.havePolishCharacters(user));
    }

    @Test
    public void isTimeCorrect(){
        ApplicationForHolidays app = new ApplicationForHolidays();
        Calendar calendar = Calendar.getInstance();
        app.setSince(calendar.getTime());
        calendar.add(Calendar.HOUR,1);
        app.setTo(calendar.getTime());

        assertTrue(programmerService.isTimeCorrect(app));
    }

    @Test
    public void changeSalary() throws Exception {
        assertEquals("3000.0", programmerService.changeSalary(applicationForHolidays.get(0).getUserWhoRequestAboutHolidays()));
    }
}